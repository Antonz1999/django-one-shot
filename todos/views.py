from django.shortcuts import render, get_object_or_404, redirect
from todos.models import TodoList, TodoItem
from todos.forms import TodoListForm, TodoItemForm


def show_todolist(request):
    list = TodoList.objects.all()
    context = {
        "todo_list": list,
    }
    return render(request, "todos/todo_list.html", context)


def show_todolist_detail(request, id):
    detail_list = get_object_or_404(TodoList, id=id)
    context = {"list_object": detail_list}
    return render(request, "todos/todo_list_detail.html", context)


def create_list(request):
    if request.method == "POST":
        form = TodoListForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect("todo_list_list")

    else:
        form = TodoListForm()

    context = {"form": form}

    return render(request, "todos/create_list.html", context)


def edit_list(request, id):
    list = get_object_or_404(TodoList, id=id)
    if request.method == "POST":
        form = TodoListForm(request.POST, instance=list)
        if form.is_valid():
            form.save()
            return redirect("todo_list_detail", id=id)

    else:
        form = TodoListForm(instance=list)

    context = {
        "list": list,
        "form": form,
    }

    return render(request, "todos/edit_list.html", context)


def delete_list(request, id):
    list = TodoList.objects.get(id=id)
    if request.method == "POST":
        list.delete()
        return redirect("todo_list_list")

    return render(request, "todos/delete_list.html")


def create_todo_item(request):
    if request.method == "POST":
        form = TodoItemForm(request.POST)
        if form.is_valid():
            todo_item_instance = form.save()
            return redirect("todo_list_detail", todo_item_instance.list.id)

    else:
        form = TodoItemForm()

    context = {"form": form}

    return render(request, "todos/create_item.html", context)


def edit_item(request, id):
    item = get_object_or_404(TodoItem, id=id)
    if request.method == "POST":
        form = TodoItemForm(request.POST, instance=item)
        if form.is_valid():
            form.save()
            return redirect("todo_list_detail", id=item.list.id)

    else:
        form = TodoItemForm(instance=item)

    context = {
        "item": item,
        "form": form,
    }

    return render(request, "todos/edit_item.html", context)
