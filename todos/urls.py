from django.urls import path
from todos.views import (
    edit_list,
    show_todolist,
    show_todolist_detail,
    create_list,
    delete_list,
    create_todo_item,
    edit_item,
)

urlpatterns = [
    path("", show_todolist, name="todo_list_list"),
    path("<int:id>/", show_todolist_detail, name="todo_list_detail"),
    path("create/", create_list, name="create_list"),
    path("<int:id>/edit/", edit_list, name="edit_list"),
    path("<int:id>/delete/", delete_list, name="delete_list"),
    path("items/create/", create_todo_item, name="create_todo_item"),
    path("items/<int:id>/edit/", edit_item, name="edit_item"),
]
